package abstractFactory;

public abstract class AbstraktiTehdas {

	abstract Farkut getFarkut();
	abstract Paita getPaita();
	abstract Lippis getLippis();
	abstract Keng�t getKeng�t();
}
