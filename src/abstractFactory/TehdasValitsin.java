package abstractFactory;

public class TehdasValitsin {

	public static AbstraktiTehdas getTehdas(String vaatteetTyyppi) {

		if (vaatteetTyyppi.equalsIgnoreCase("boss")) {
			return new BossTehdas();

		} else if (vaatteetTyyppi.equalsIgnoreCase("adidas")) {
			return new AdidasTehdas();

		}
		return null;
	}

}
