package abstractFactory;

public class AdidasTehdas extends AbstraktiTehdas {

	@Override
	public Farkut getFarkut() {
		return new AdidasFarkut();
	}

	@Override
	public Paita getPaita() {
		return new AdidasPaita();
	}

	@Override
	public Lippis getLippis() {
		return new AdidasLippis();
	}

	@Override
	public Keng�t getKeng�t() {
		return new AdidasKeng�t();
	}
}
