package abstractFactory;

public class Jasper {
	public static void main(String[] args) {
		
		Farkut farkut = null;
		Paita paita = null;
		Lippis lippis = null;
		Keng�t keng�t = null;
		
		
		AbstraktiTehdas vaateTehdas = TehdasValitsin.getTehdas("Adidas");
		
		System.out.println("Opiskelija Jasperilla on p��ll� ");
		farkut = vaateTehdas.getFarkut();
		paita = vaateTehdas.getPaita();
		lippis = vaateTehdas.getLippis();
		keng�t = vaateTehdas.getKeng�t();
		
		System.out.println(farkut.toString());
		System.out.println(paita.toString());
		System.out.println(lippis.toString());
		System.out.println(keng�t.toString());
		
		System.out.println("Jasper valmistuu koulusta ja pukee p��lleen ");
		vaateTehdas = TehdasValitsin.getTehdas("Boss");
		
		farkut = vaateTehdas.getFarkut();
		paita = vaateTehdas.getPaita();
		lippis = vaateTehdas.getLippis();
		keng�t = vaateTehdas.getKeng�t();
		
		System.out.println(farkut.toString());
		System.out.println(paita.toString());
		System.out.println(lippis.toString());
		System.out.println(keng�t.toString());
		
}
	
}