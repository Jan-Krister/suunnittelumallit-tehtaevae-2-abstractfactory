package abstractFactory;

public class BossTehdas extends AbstraktiTehdas {

	@Override
	Farkut getFarkut() {
		return new BossFarkut();
	}

	@Override
	Paita getPaita() {
		return new BossPaita();
	}

	@Override
	Lippis getLippis() {
		return new BossLippis();
	}

	@Override
	Keng�t getKeng�t() {
		return new BossKeng�t();
	}

	
}
